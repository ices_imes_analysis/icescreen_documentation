---
title: "ICE line format"
date: 2018-12-29T11:02:05+06:00
lastmod: 2025-02-10T10:42:26+06:00
weight: 3
draft: false
# search related keywords
keywords: [""]
---


The ICE line format characterizes the arrangement and the distance between signature proteins (SP) within an ICE/IME element.
Example of ICE line format : C4.V5:C2.R7.V1:I1:R4.I1!i2!i7.R1:v6.c5!R2.I2!R2.I
 - the letters characterize the SP themselves :
	 - type: R = Relaxase, C = Coupling Protein, V = VirB4, I = Integrase, D = DDE
	 - uppercase or lowercase: UPPERCASE = SP on forward strand, lowercase = SP on reverse strand
 - the approximate number of CDSs between SPs is characterized by a number followed by a symbol. The symbol represents the muliplier to calculate the approximate number of the CDSs:
	. means the number of CDS separating the SPs is single digit. The number before this symbol is multiply by 1 to find the approximate amount of the CDSs.
	: means dozens of CDSs are separating the SPs. The number before this symbol is multiply by 10 to find the approximate amount of the CDSs.
	! means hundreds of CDSs are separating the SPs. The number before this symbol is multiply by 100 to find the approximate amount of the CDSs.
In the example above, the first coupling protein is separated from the next VirB4 by 4 CDS. The VirB4 is separated from the next coupling protein by more than 50 CDS but less than 60.
