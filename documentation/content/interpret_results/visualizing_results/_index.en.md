---
title: "Visualizing the results"
date: 2018-12-29T11:02:05+06:00
lastmod: 2025-01-20T10:42:26+06:00
weight: 4
draft: false
# search related keywords
keywords: [""]
---

 - User can visualize the mobile elements annotations using a genome visualization software such as Artemis, JBrowse, or IGV. See the sub-section "standard genome annotation formats" of the section "Description of the output files" for more details on the content of those files.
 - A R script can also be developed (not included) to create a visualisation which display symbols and make abstraction of the genomic distances to provide an overview of the arrangements of the mobile elements :

<img src="example_visualisation_symbols.png"/>


