---
title: "Interpret the results"
date: 2020-12-30T11:02:05+03:00
lastmod: 2025-01-10T10:42:26+06:00
icon: "ti-stats-up"
description: "description of the output files."
type : "docs"
---

Description of the output files and how to interpret them.
