---
title: "Main features of ICEscreen"
date: 2018-12-29T11:02:05+06:00
lastmod: 2025-01-18T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["features", "tool"]
---

ICEscreen aims to detect ICEs and IMEs in Bacillota genomes:
* Detection of signature proteins (SPs) of ICEs/IMEs by using blastP on a curated resource. BlastP allows for an accurate assignment of hits to a given ICE/IME superfamily or family. The curated resource was derived from an analysis of over 120 ICEs and IMEs in Streptococcus genomes by the DINAMIC lab at Nancy France.
* Detection of distant homologs of SPs by using HMM profiles of ICEs/IMEs protein families. The HMM profiles have been either imported from trusted resources or created and curated when needed.
* Detection of the ICE/IME structures: ICEScreen groups together SPs that belong to the same ICE/IME structures to the best of its ability.
* Currently, ICEscreen does not actually delimit the ICE and IME elements, it only assembles ICE and IME signature proteins (Relaxase, VirB4, Coupling, and Integrase) into element structures. Usually if an Integrase is found, it is assumed to be one of the boundary but the other boundary is harder to detect automatically. ICEscreen only gives the coordinates of the most upstream and most downstream signature proteins for each element, see the columns "Start_of_most_upstream_SP" and "Stop_of_most_downstream_SP" of the output file *_detected_ME.tsv. But the actual ICE and IME boundaries may extend beyond those coordinates. The actual ICE and IME boundaries can be found manually by either looking for the direct repeat sequences (DRs) or comparing closely related genomes with and without the ICE and IME element. If a cargo genes of interest is comprised within the "Start_of_most_upstream_SP" and "Stop_of_most_downstream_SP", one can assume that it is carried by the ICE or IME elements. If a cargo genes of interest is located outside but at proximity of the "Start_of_most_upstream_SP" or the "Stop_of_most_downstream_SP", a manual delimitation of ICE and IME boundaries is necessary to known if this genes is carried by the ICE or IME elements or not.



