---
title: "Scientific background"
date: 2018-12-29T11:02:05+06:00
lastmod: 2025-01-23T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: [""]
---

ICEScreen aims to detect mobile genetic elements ICEs and IMEs in Bacillota.

### What is the general context regarding mobile genetic elements?
Horizontal gene transfers constitute a major evolutionary force among bacterial genomes (Frost et al., 2005; Treangen and Rocha, 2011). Mobile genetic elements can transfer from one cell to another and carry fitness genes which can increase the adaptability or resilience of their host to a given environment. Mobile genetic elements are suspected to be heavily involved in the spread of antibiotic resistance for example, which is a major public health problem.

### Why the Bacillota?
Bacillota contain more than 250 bacterial genera including Streptococcus, Staphylococcus, Lactobacillus, Enterococcus, Bacillus, Listeria or Clostridium. They are bacteria found in a large number of ecosystems, such as environmental, food, or in the microbiota of animals and humans. Some strains are considered as probiotic, commensal or pathogenic (i.e. Enterococcus, Streptococcus, Staphylococcus, Mycoplasma).

### What are ICEs and IMEs exactly?
ICE stands for Integrative and conjugative elements, and IME stands for integrative and mobilizable elements. ICEs and IMEs belong to the family of conjugative mobile elements. Conjugation is a one-way transmission mechanism of DNA from a donor cell to a recipient cell. ICEs encode their own excision and transfer by conjugation to a recipient bacterium as well as their integration into the host chromosome. In some cases, conjugation can also promote the transfer of other non-autonomous elements. IMEs encode all the functions necessary for their excision and integration but unlike ICEs they are not autonomous for their transfer. In other words, IMEs need the conjugation machinery of another conjugative element (plasmid or ICEs) from the same cell to transfer. At the moment the mechanistics of the transfer of ICEs and IMEs in Bacillota are not fully understood. However it is hypothesized that this transfer mechanism shares similarities with the one of conjugative plasmids. The genes and the sequences involved in the same biological function are called a functional module. There are four main modules for ICEs: recombination, conjugation, regulation and fitness. Each ICE possesses only one module of each type with the exception that there can be multiple fitness modules. IMEs possess a recombination module, a mobilization module and one or more fitness modules.

##### The recombination module
The recombination module encodes the enzymes and sequences required for the integration and excision of the mobile element into the genomes. A circular intermediary of the ICE or the IME is generated in the process. Three types of recombination modules have been observed in Bacillota: modules encoding a tyrosine integrase, modules encoding one or more serine integrases and modules encoding a DDE transposase. Insertion into a genome by recombination generates direct repeats (DR) flanking the element. For known ICEs and IMEs in Bacillota, these DR range in size from 8 to 100 nt. The DR sequences can be exactly identical or have mismatches.

##### The conjugation modules
The ICE conjugation module performs two different transfer functions encoded each by their own sub-modules: (1) DNA mobilization for transfer to the recipient cell and (2) setting up of the conjugation pore. The transfer of the single-strand DNA is likely initiated by the relaxosome, a multiprotein complex which recognizes the origin of transfer (oriT) of the ICE. The main protein in the relaxosome is the relaxase which cleaves the single-strand DNA at the nick site (Alvarez-Martinez and Christie, 2009). The transfer of the DNA-relaxase complex is carried out by a coupling protein which is thought to initiate passage through the conjugation pore (Guglielmini et al., 2013). Known coupling proteins in Bacillota are VirD4 and TcpA. The conjugation pore is a multiprotein complex related to the type IV secretion system (T4SS). It ensures the transport of the DNA-relaxase complex through the cell membranes of both the donor and the recipient bacteria. This transport requires energy supplied by the T4SS ATPases. The best known T4SS ATPases is the VirB4 (Alvarez-Martinez and Christie, 2009). The IME mobilization module contains only part of the genes involved in the ICE conjugation module, and interestingly IMEs never encode a VirB4 protein.

##### The regulatory module
The regulatory module often contains proteins related to those of prophages. It may be absent in IMEs.

##### The adaptation modules
The adaptation modules of ICEs and IMEs have genes which are not involved in the life cycle of the element but which can confer different traits to the host organism. Those traits can provide a selective advantage to the host such as resistance to antibiotics, resistance to heavy metals, virulence, symbiosis, etc.

### How to classify ICEs and IMEs?
ICEs classification in ICEScreen is based on the conjugation module. This type of classification is similar to the one enacted by the scientific community on conjugative and mobilizable plasmids. It takes into account the relaxase (Francia et al., 2004; Garcillán-Barcia et al., 2009, 2011) and the mating pore proteins (Smillie et al., 2010). There are three known ICE superfamilies in streptococci: Tn916, Tn5252, and TnGBS1. The classification of IMEs is based solely on the relaxase (Coluzzi et al., 2017), and includes nine superfamilies.

### ICEs and IMEs can have complexe structures
Genomic islands can have a composite structure (Wozniak and Waldor, 2010; Bellanger et al., 2014) made up of several mobile elements such as ICEs, IMEs, transposons, ect. These mobile elements can be inserted into each other (nested) or located side by side (tandem). There can be one or multiple levels of nesting.

### History of ICEScreen
ICEScreen is based on the tool ICE/IME Finder that was developed to detect ICEs and IMEs in Streptococcus genomes by the DYNAMIC team (Ambroset et al., 2016; Coluzzi et al., 2017). ICEScreen takes this approach further in two directions: generalization to Bacillota and detection of the structures. The main features of the tool are describes further in the next section.

### Other tools that detect ICEs and IMEs
At the moment, the tools that detect ICEs and IMEs are imperfect. Our knowledge of the different families of SPs of ICEs and IMEs is still partial and many factors render the detection of some ICEs and IMEs elusive: some elements are decayed, some functional modules are shared with phages and plasmids, and some elements have inextricable composite structures. Please find below a list of tools other than ICEScreen that detect ICEs and IMEs:
* ICEfinder is the tool associated with the ICEberg2 database (Liu et al., 2019). Searching for SPs is the first step common to the three methods, although the types of SPs considered for are not exactly identical. The ICEfinder tool detects and annots ICEs and IMEs in all bacterial genomes. It searches for SPs by using BlastP, HMMER et oriTfinder (Li et al., 2018). When the ICE or IME is integrated into a tRNA, ICEfinder uses ARAGORN and Vmatch to delimitate at the nucleotide level. ICEfinder restricts its search for IMEs that are maximum 50kb of size.
* CONJscan is used to find conjugation modules and has a module to detect ICEs specifically (Cury et al., 2020, Abby et al., 2016). CONJscan detects conjugation systems bounded by core genome genes which cannot be derived from horizontal transfers. For this delimitation to be effective, CONJscan uses the tools Roary and USEARCH and requires at least 4 closely-related genomes of which some harbor the mobile element and some don't. CONJscan provides the CONJ model which detects ICEs (relaxase + VirB4 + coupling protein). It looks for additional proteins of the Mating Pair Formation and requires two of them to be present in addition to the SPs mentioned above in order for an element to be considered valid. CONJscan is not made to detect IME but an unpublished MOB model exists which detects elements with relaxase + possibly VirB4 + possibly coupling protein + two proteins of the Mating Pair Formation. Some of these elements can be considered as IMEs, others as non-functional ICE derivatives.




