---
title: "The algorithm in details"
date: 2018-12-29T11:02:05+06:00
lastmod: 2025-01-17T10:42:26+06:00
weight: 3
draft: false
# search related keywords
keywords: ["description", "algorithm"]
---

The algorithm of ICEscreen for finding ICEs and IMEs is divided into 2 modules: (1) detection of the signature proteins and (2) detection of the structures.
