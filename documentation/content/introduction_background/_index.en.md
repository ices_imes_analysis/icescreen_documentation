---
title: "Presentation of the tool"
date: 2020-12-30T11:02:05+01:00
lastmod: 2025-01-19T10:42:26+06:00
icon: "ti-book"
description: "Would you like a scoop of ICEscreen for your genomes?"
type : "docs"
---

ICEscreen is a bioinformatic pipeline for the detection and annotation of mobile genetic elements ICEs (Integrative and Conjugative Elements) and IMEs (Integrative and Mobilizable Elements) in Bacillota genomes. The forge of the project is accessible at https://forgemia.inra.fr/ices_imes_analysis/icescreen.
