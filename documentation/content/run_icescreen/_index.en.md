---
title: "Install / run ICEscreen"
date: 2020-12-30T11:02:05+02:00
lastmod: 2025-01-10T10:42:26+06:00
icon: "ti-settings"
description: "Run ICEscreen locally or remotely with the different options."
type : "docs"
---

How to (1) install and run ICEscreen locally or (2) run remotely without installation.
