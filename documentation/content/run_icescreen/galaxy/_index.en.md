---
title: "Run remotely with Galaxy"
date: 2018-12-29T11:02:05+06:00
lastmod: 2025-01-05T10:42:26+06:00
weight: 2
draft: false
# search related keywords
keywords: ["Run","Galaxy"]
---


## ICEscreen is available in Galaxy

The repository `icescreen` is available to install from the Galaxy main toolshed (https://toolshed.g2.bx.psu.edu/). You can ask your Galaxy admin to install it in their local Galaxy instance, the tool will then be available under the `Genome annotation` section. If you have a Migale account at https://galaxy.migale.inra.fr/, you can test ICEscreen there.


### Issues
Please check or report any issues with the software on https://forgemia.inra.fr/ices_imes_analysis/icescreen

