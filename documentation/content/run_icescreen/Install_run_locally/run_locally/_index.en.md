---
title: "Run / command line"
date: 2018-12-29T11:02:05+06:00
lastmod: 2024-01-25T10:42:26+06:00
weight: 4
draft: false
# search related keywords
keywords: ["run","locally"]
---




### Running ICEscreen locally on your genbank files

ICEscreen requires input files in genbank format. Multigenbank files (i.e. gbff files featuring multiple genome records back to back) are supported. Each Genbank record must include the ORIGIN nucleotide sequence. Only genbank files that are at the root of the input genbank directory are taken into account (not the one if nested subdirectories). A command line for running ICEscreen with basic options looks like this:
```shell
icescreen -g path/to/folder/containing/genbank/files/to/analyse -o /path/to/the/output/directory -j NUMBER_OF_PARALLELIZED_JOBS --phylum TAXONOMIC_GROUP
```
If you are unsure about what job parallelization means, set the -j option to 1 or skip the -j option entirely. An example of --phylum TAXONOMIC_GROUP is: --phylum bacillota.



