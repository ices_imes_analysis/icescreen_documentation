---
title: "Troubleshooting the installation"
date: 2018-12-29T11:02:05+06:00
lastmod: 2024-01-25T10:42:26+06:00
weight: 3
draft: false
# search related keywords
keywords: ["Troubleshooting"]
---

### Troubleshooting the installation

#### Reporting a new issue
If this section didn't help you resolve your problem, please report your issue on the [forge of the project.](https://forgemia.inra.fr/ices_imes_analysis/icescreen). You can create a new issue in [Plan -> Issues](https://forgemia.inra.fr/ices_imes_analysis/icescreen/-/issues). You can also contact us at icescreen-contact AT inrae.fr.

#### Snakemake error: clock skew problem in your network

This error can happen at a random point during the execution of the pipeline, mostly when the pipeline is run on a cluster. This issue is related to https://github.com/snakemake/snakemake/issues/3261.
Workaround: revert to a version of snakemake-minimal that does not contain this bug, for example snakemake-minimal=8.18.0:
```
conda activate icescreen_env_1-3-3
conda install snakemake-minimal=8.18.0
```

#### UnsatisfiableError (installation with Conda)

Example of `UnsatisfiableError`:

```shell
UnsatisfiableError: The following specifications were found to be incompatible with each other:
Output in format: Requested package -> Available versionsThe following specifications were found to be incompatible with your system:
  - feature:/linux-64::__glibc==2.31=0
  - python=3.9 -> libgcc-ng[version='>=7.5.0'] -> __glibc[version='>=2.17']
Your installed version is: 2.31
```

If you experience this error, it is likely related to the configuration of the Conda channels. In order to install the dependencies of ICEscreen, the conda-forge and bioconda channels both must be specified to Conda (`-c conda-forge -c bioconda`). Run the install command as follow:
```shell
conda install -c conda-forge -c bioconda icescreen
```

Alternatively, you can add the channels at the global level in Conda:
```shell
conda config --get channels
```
should output:
```shell
--add channels 'conda-forge'   # lowest priority
--add channels 'bioconda'
--add channels 'defaults'   # highest priority
```
If not, you can add the channels like so:
```shell
conda config --append channels conda-forge
conda config --add channels bioconda
conda config --add channels defaults
```
If that doesn't work, you can also try using Mamba (https://anaconda.org/conda-forge/mamba) or disable channel priority altogether by running:
```shell
conda config --set channel_priority false
```

#### BLAST Database error: Error pre-fetching sequence data

This error likely arises because the indexing of the blast resources was carried out by a version of blast incompatible with the version of blast that performs the genomic comparison. To fix this issue, run the following command from a terminal:
```shell
icescreen --index_genomic_resources
```
Then delete the output directory generated by the previous run and re-run the main icescreen command. For the example dataset downloaded from NCBI described in `Testing your local installation`:
```shell
rm -rf ~/tmp/test_icescreen/ICEscreen_results/
icescreen -g ~/tmp/test_icescreen/genbank_files -o ~/tmp/test_icescreen/ --phylum bacillota
```
This error should not happen if ICEscreen was installed in a specific and isolated Conda environment (see `Install with Conda`).

#### ModuleNotFoundError: No module named 'BCBio'

The bcbio-gff is missing, manually install it in the icescreen conda environment, see https://anaconda.org/bioconda/bcbio-gff for details. This error should not happen if ICEscreen was installed in a specific and isolated Conda environment (see `Install with Conda`).

#### AttributeError: 'str' object has no attribute 'name'

snakemake-minimal-7.15.1 caused an error on october 2022 : AttributeError: 'str' object has no attribute 'name'. For details, see https://github.com/snakemake/snakemake/issues/1892. snakemake-minimal-7.15.2 was released 3 days latter and resolved the issue. If you experience this issue, please upgrade snakemake-minimal to a version greater than 7.15.1.

