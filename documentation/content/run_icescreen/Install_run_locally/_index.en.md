---
title: "Install and run ICEscreen locally"
date: 2018-12-29T11:02:05+06:00
lastmod: 2024-01-23T10:42:26+06:00
weight: 1
draft: false
# search related keywords
keywords: ["Install","run","locally"]
---

There are two ways to install ICEscreen on your computer: (1) installation from source, or (2) installation from Conda

