---
title: "Contact"
draft: false
---

<img src= "themify_email.svg" />&nbsp;&nbsp;Email: icescreen-contact **AT** inrae.fr

<img src= "themify_comments.svg" />&nbsp;&nbsp;Link to the gitlab forge / report issues: https://forgemia.inra.fr/ices_imes_analysis/icescreen

<img src= "themify_medall.svg" />&nbsp;&nbsp;Citation: ICEscreen is developed by J. Lao, T. Lacroix, C. Coluzzi, G. Guédon, N. Leblond-Bourget and H. Chiapello from the University of Lorraine DynAMic and the INRAE MaIAGE research teams. Thank you for citing our latest publication, see the home page of https://icescreen.migale.inrae.fr for details.
