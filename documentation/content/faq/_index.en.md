---
title: "Frequently Asked Questions"
draft: false
---

{{< faq "What is the licence of ICEscreen?" >}}
The licence of ICEscreen is GNU Affero General Public License or GNU AGPL ( https://www.gnu.org/licenses/agpl-3.0.en.html ). It is the most widely used free license with copyleft and obligation to provide the community with modified versions.
![agplv3](./agplv3-with-text-162x68.png)
{{</ faq >}}

{{< faq "How can I report a bug or ask for a new feature?" >}}
Please use the [forge of the project.](https://forgemia.inra.fr/ices_imes_analysis/icescreen). You can create a new issue in [Plan -> Issues](https://forgemia.inra.fr/ices_imes_analysis/icescreen/-/issues).
{{</ faq >}}

{{< faq "How can I contact the ICEscreen team?" >}}
You can contact us at icescreen-contact AT inrae.fr.
{{</ faq >}}

{{< faq "Can I use ICEscreen on my genome?" >}}
ICEscreen is parametred to find ICE and IME mobile elements in Bacillota only.
{{</ faq >}}
